## openfoam-docker

This repository contains two scripts that allow to install and run docker images of various versions of OpenFOAM.

The supported versions are referred to as follows.
- Foundation releases: 4.1 5 6 7
- OpenCFD releases: 3+ and vYYMM for the follwoing.

To install a particular version run the install script with the desired as the first argument.
For example

`./installOpenFOAM 3+`

to install the 3+ version from OpenCFD.
Similarly to run the isntalled container

`startOpenFOAM 3+`

Scripts were tested on Ubuntu, the docker commands in the scripts are adapted from that provided by OpenCFD and the OpenFOAM Foundation.

**This offering is not approved or endorsed by OpenCFD Limited, producer and distributor of the OpenFOAM software via www.openfoam.com, and owner of the OPENFOAM and OpenCFD trademarks.**
